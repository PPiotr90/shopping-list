package pl.pp.shopping.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DtoProduct {

    private String value;
}
