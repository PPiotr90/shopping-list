package pl.pp.shopping.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DtoChangePassword {
    private String oldPassword;
    private String NewPassword;
}
