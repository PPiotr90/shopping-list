package pl.pp.shopping.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DtoUser {

    private String username;
    private String password;
}
