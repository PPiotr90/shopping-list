package pl.pp.shopping.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;

@NoArgsConstructor
@Data
public class DtoProductsToChange {


   private HashMap<String, Boolean> positions = new HashMap<>();

}
