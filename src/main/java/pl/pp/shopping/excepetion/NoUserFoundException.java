package pl.pp.shopping.excepetion;

public class NoUserFoundException extends RuntimeException {

    public NoUserFoundException(String m) {
        super(m);

    }
}
