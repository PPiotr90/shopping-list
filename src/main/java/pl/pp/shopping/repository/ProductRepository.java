package pl.pp.shopping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.pp.shopping.entity.Product;

import java.util.Optional;


@Transactional
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    public Optional<Product> findProductByValue(String value );
}
