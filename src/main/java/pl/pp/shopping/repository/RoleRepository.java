package pl.pp.shopping.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.pp.shopping.entity.Role;


@Repository
@Transactional
public interface RoleRepository  extends PagingAndSortingRepository<Role, Long> {
//    /aaaa
}
