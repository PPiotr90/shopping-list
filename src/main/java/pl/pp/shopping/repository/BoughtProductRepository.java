package pl.pp.shopping.repository;


import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import pl.pp.shopping.entity.BoughtProduct;

@Repository
public interface BoughtProductRepository  extends PagingAndSortingRepository<BoughtProduct, Long> {

}
