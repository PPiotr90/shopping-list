package pl.pp.shopping.entity;


import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "username", unique = true, nullable = false)
    private String userName;
    @Column(nullable = false)
    private String password;
    @Column(name = "enabled")
    private int enabled;

    @Column
    LocalDateTime lastLogin;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "user_product",
            joinColumns = {@JoinColumn(name = "userId")},
            inverseJoinColumns = {@JoinColumn(name = "productId")})
    private List<Product> products;
    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "user_boughtProduct",
            joinColumns = {@JoinColumn(name = "userId")},
            inverseJoinColumns = {@JoinColumn(name = "bought_productId")})
    private List<BoughtProduct> boughtProducts;

    public User() {
        products = new ArrayList<>();
        boughtProducts = new ArrayList<>();
    }

    public void addProduct(Product product) {

        if (!products.contains(product)) products.add(product);
    }

    public void addBoughtProducts(List<BoughtProduct> boughtProductsToSave) {
        boughtProducts.addAll(boughtProductsToSave);

    }


}





