package pl.pp.shopping.entity;


import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "boughtProducts")
@Data
public class BoughtProduct{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column( nullable = false)
    private String value;
@Column(nullable = false)
    private LocalDate localDate;

    BoughtProduct(String value) {
        this.setValue(value);
        localDate = LocalDate.now();
    }

    BoughtProduct() {
        localDate = LocalDate.now();
    }

    public BoughtProduct(Product product) {
        this(product.getValue());
    }
}
