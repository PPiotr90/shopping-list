package pl.pp.shopping.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pp.shopping.entity.User;
import pl.pp.shopping.excepetion.NoUserFoundException;
import pl.pp.shopping.repository.UserRepository;

@Service
public class UserService {

    private UserRepository userRepository;


    @Autowired
    UserService(UserRepository userRepository) {
        this.userRepository = userRepository;

    }

    public User findUserByUserName(String userName) {
        User result = userRepository.findUserByUserName(userName).orElse(null);
if (result.equals(null)) {
    throw new NoUserFoundException("user not found");
}
else return result;
    }

    public User save(User userToSave) {
        return userRepository.save(userToSave);
    }
}
