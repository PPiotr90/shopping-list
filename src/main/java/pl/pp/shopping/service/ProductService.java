package pl.pp.shopping.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pp.shopping.entity.Product;
import pl.pp.shopping.repository.ProductRepository;

@Service
public class ProductService {
    private ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product findProductByValue(String value) {
        Product result = productRepository.findProductByValue(value).orElse(null);
        return result;

    }

    public Product save(Product product) {
        return productRepository.save(product);
    }


}