package pl.pp.shopping.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pp.shopping.entity.BoughtProduct;
import pl.pp.shopping.repository.BoughtProductRepository;

import java.util.ArrayList;

@Service
public class BoughtProductService {

    @Autowired
    private BoughtProductRepository boughtProductRepository;

    public Iterable<BoughtProduct> saveAll(ArrayList<BoughtProduct> boughtProducts) {
        return boughtProductRepository.saveAll(boughtProducts);
    }

    public void deleteById(Long id) {
        boughtProductRepository.deleteById(id);
    }
}
