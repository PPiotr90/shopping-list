package pl.pp.shopping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.pp.shopping.entity.BoughtProduct;
import pl.pp.shopping.entity.User;
import pl.pp.shopping.service.BoughtProductService;
import pl.pp.shopping.service.UserService;
import pl.pp.shopping.utility.Utilities;

import java.util.List;

@Controller
@RequestMapping("/lastShopping")
public class LastShoppingController {

    private UserService userService;
   private BoughtProductService boughtProductService;

    @Autowired
    LastShoppingController (UserService userService, BoughtProductService boughtProductService) {
        this.userService = userService;
        this.boughtProductService = boughtProductService;
    }


    @GetMapping
    public String getLastShopping(Model model, Authentication authentication) {
        final User loggedUser = Utilities.findLoggedUser(authentication, userService);
        final List<BoughtProduct> boughtProducts = loggedUser.getBoughtProducts();
        model.addAttribute("boughtProducts", boughtProducts);

        return "lastShopping";

    }



}
