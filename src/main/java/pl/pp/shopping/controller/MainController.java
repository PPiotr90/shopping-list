package pl.pp.shopping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cassandra.CassandraProperties;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.pp.shopping.dto.DtoUser;
import pl.pp.shopping.entity.Role;
import pl.pp.shopping.entity.User;
import pl.pp.shopping.service.RoleService;
import pl.pp.shopping.service.UserService;

@Controller
@RequestMapping("/")
public class MainController {

    UserService userService;
    RoleService roleService;

    @Autowired
    public MainController(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }


    @GetMapping()
    public String home() {
        return "index";
    }

    @GetMapping("/login")
    public String getLogin() {
        return "login";
    }

    @GetMapping("/create")
    public String getCreatePage(Model model) {
        DtoUser dtoUser = new DtoUser();
        model.addAttribute("newUser", dtoUser);

        return "create";
    }

    @PostMapping("/create/new")
    public String createUser(@ModelAttribute("newUser") DtoUser userFromForm) {
        User userToSave = new User();
        userToSave.setEnabled(1);
        userToSave.setPassword(userFromForm.getPassword());
        userToSave.setUserName(userFromForm.getUsername());

        Role roleToSave = new Role();
        roleToSave.setAuthority("ROLE_USER");
        roleToSave.setUsername(userToSave.getUserName());

        roleService.save(roleToSave);
        userService.save(userToSave);

        return "redirect:/";
    }

    @PostMapping("/access-denied")
    public String showAccessDenied() {

        return "access-denied";

    }


}
