package pl.pp.shopping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.pp.shopping.dto.DtoChangePassword;
import pl.pp.shopping.dto.DtoProduct;
import pl.pp.shopping.dto.DtoProductsToChange;
import pl.pp.shopping.entity.BoughtProduct;
import pl.pp.shopping.entity.Product;
import pl.pp.shopping.entity.User;
import pl.pp.shopping.service.BoughtProductService;
import pl.pp.shopping.service.ProductService;
import pl.pp.shopping.service.UserService;
import pl.pp.shopping.utility.Utilities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    private static final int MAX_SIZE_BOUGHT_LIST = 10;
    UserService userService;
    ProductService productService;
    BoughtProductService boughtProductService;

    @Autowired
    UserController(UserService userService, ProductService productService,
                   BoughtProductService boughtProductService) {

        this.userService = userService;
        this.productService = productService;
        this.boughtProductService = boughtProductService;
    }

    @GetMapping
    public String UserPage(Model model, Authentication authentication) {
        final User loggedUser = Utilities.findLoggedUser(authentication, userService);
        loggedUser.setLastLogin(LocalDateTime.now());
        final List<Product> productList = loggedUser.getProducts();
        model.addAttribute("productList", productList);
        userService.save(loggedUser);

        DtoProduct productToList = new DtoProduct();
        model.addAttribute("productToBuy", productToList);

        DtoProductsToChange productsToChange = new DtoProductsToChange();


        model.addAttribute("positions", productsToChange);

        return "user-page";
    }


    @GetMapping("/password")
    public String getChangePassword(Model model) {
        DtoChangePassword dtoChangePassword = new DtoChangePassword();
        model.addAttribute("changePassword", dtoChangePassword);
        return "changePassword";
    }


    @GetMapping("/password/change")
    public String patchChangePassword(@ModelAttribute("changePassword") DtoChangePassword dtoChangePassword,
                                      Authentication authentication) {
        final User loggedUser = Utilities.findLoggedUser(authentication, userService);
        if (dtoChangePassword.getOldPassword().equals(loggedUser.getPassword())) {
            loggedUser.setPassword(dtoChangePassword.getNewPassword());
            userService.save(loggedUser);
        }

        return "redirect:/user";
    }

    @PostMapping("/addProduct")
    public String addProductToList(@ModelAttribute("productToBuy") DtoProduct productFromWeb,
                                   Authentication authentication) {
        String value = productFromWeb.getValue();
        final User loggedUser = Utilities.findLoggedUser(authentication, userService);
        String separateChar = new String();
        if (value.contains(";")) separateChar = ";";
        else if (value.contains(",")) separateChar = ",";
        if (!"".equals(separateChar)) {
            String[] products = value.split(separateChar);
            for (String product : products) {
                addProductToUser(product, loggedUser);
            }
        } else {
            addProductToUser(value, loggedUser);
        }

        return "redirect:/user";
    }

    private void addProductToUser(String value, User loggedUser) {
        value = value.trim();
        value = value.toLowerCase();
        Product productToList = productService.findProductByValue(value);

        if (productToList == null) {
            productToList = new Product(value);
        }
productToList.setLastUsing(LocalDateTime.now());
        productService.save(productToList);
        loggedUser.addProduct(productToList);
        userService.save(loggedUser);
    }

    @GetMapping("/change")
    public String deleteFromList(@ModelAttribute("positions") DtoProductsToChange dtoProductsToChange,
                                 Authentication authentication,
                                 @RequestParam String action) {
        final User loggedUser = Utilities.findLoggedUser(authentication, userService);
        List<Product> productsOfUser = loggedUser.getProducts();
        List<BoughtProduct> usersBoughtProducts = loggedUser.getBoughtProducts();
        final HashMap<String, Boolean> elements = dtoProductsToChange.getPositions();
        ArrayList<BoughtProduct> boughtProducts = new ArrayList<>();
        for (String s : elements.keySet()) {
            final Boolean t = true;
            if (t.equals(elements.get(s))) {
                Product productToDelete = productService.findProductByValue(s);
                productsOfUser.remove(productToDelete);
                if (action.equals("bought")) {
                    BoughtProduct boughtProduct = new BoughtProduct(productToDelete);
                    boughtProducts.add(boughtProduct);
                }
            }
        }
        if (action.equals("bought")) {
            usersBoughtProducts.addAll(boughtProducts);

        }
        boughtProductService.saveAll(boughtProducts);
        while (usersBoughtProducts.size() > MAX_SIZE_BOUGHT_LIST) {
            usersBoughtProducts = deleteFirstBoughtProduct(usersBoughtProducts);
        }
        loggedUser.setBoughtProducts(usersBoughtProducts);
        loggedUser.setProducts(productsOfUser);
        userService.save(loggedUser);


        System.out.println("hi in delete method");

        return "redirect:/user";
    }

    private List<BoughtProduct> deleteFirstBoughtProduct(List<BoughtProduct> usersBoughtProducts) {
        BoughtProduct firstBoughtProduct = usersBoughtProducts.get(0);
        for (BoughtProduct product : usersBoughtProducts) {
            if (firstBoughtProduct.getId() > product.getId()) {
                firstBoughtProduct = product;

            }
        }
        usersBoughtProducts.remove(firstBoughtProduct);
        boughtProductService.deleteById(firstBoughtProduct.getId());
        return usersBoughtProducts;
    }

}
