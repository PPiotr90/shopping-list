package pl.pp.shopping.utility;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import pl.pp.shopping.entity.Product;
import pl.pp.shopping.entity.User;
import pl.pp.shopping.service.UserService;

import java.util.List;

@Component
public class Utilities {



    public static User findLoggedUser(Authentication authentication, UserService userService) {
        final String userName = authentication.getName();
        final User loggedUsername = userService.findUserByUserName(userName);
        return loggedUsername;

    }

    public  static List<Product> deleteFromProductListByValue(List<Product> products, String value) {
        Product productToDelete = new Product(value);
        products.remove(productToDelete);
        return products;
    }
    }


